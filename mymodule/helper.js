const url = require('url');
const fs = require('fs');

function renderHtml(path, res) {
    fs.readFile(path, (err, data) => {
        if (err) {
            res.writeHead(404);
            res.write('File not found!');
        } else {
            res.write(data);
        }

        res.end();
    });

}

function render404(res) {
    res.writeHead(404);
    res.write('File not found!');
    res.end();

}

function onRequest(req, res) {
    const path = url.parse(req.url).pathname;

    switch (path) {
        case '/':
            renderHtml('./views/home.html', res);
            break;
        case '/about':
            renderHtml('./views/abount.html', res);
            break;
        default:
            render404(res);
            break;
    }
}

module.exports = {
    onRequest : onRequest
};