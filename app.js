const http = require('http');

const moduleConfig = require('./mymodule/libs.js');
const helper = require('./mymodule/helper.js');

const server = http.createServer(helper.onRequest );

server.listen(moduleConfig.port, moduleConfig.hostname, () => {

    console.log(`Server running ... http://${moduleConfig.hostname}:${moduleConfig.port}/`);
});