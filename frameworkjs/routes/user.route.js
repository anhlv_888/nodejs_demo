var express = require('express');
var router = express.Router();
var userValidate = require('../validates/user.validates.js')
var userController = require('../controllers/user.controller.js');
var multer = require('multer');
var upload = multer({ dest: './public/uploads' });


router.get('/', userController.index);


router.get('/search',userController.search);

router.get('/create', userController.getCreate);

router.get('/:id', userController.view);


// method post

router.post('/create',
    upload.single('avatar'),
    userValidate.postCreate,
    userController.postCreate
);

module.exports = router;