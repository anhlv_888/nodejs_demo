require('dotenv').config();

var express = require('express');
var bodyPaser = require('body-parser');
var db = require('./db.js');
var userRouter = require('./routes/user.route.js');
var authRouter = require('./routes/auth.route.js');
var transferRouter = require('./routes/transfer.route');
var cookieParser = require('cookie-parser');
var authMiddleware = require('./middlewares/auth.middleware.js');
var csurf = require('csurf');
var mongoose = require('mongoose');
var apiUserRoute = require('./api/routes/user.route.js');

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true });


var app = express();
var port = 3000;
var path = require('path');




app.set('view engine', 'pug');
app.set('views', './views');

app.use(bodyPaser.json());
app.use('/api/users', apiUserRoute);
app.use(cookieParser(process.env.SESSION_SECRET));
app.use(bodyPaser.urlencoded({extends : true}));
app.use(express.static('public'))
// app.use(csurf({ cookie: true }));


app.get('/', authMiddleware.requireAuth, function (req, res) {
    res.render('index', {
        name: 'viet anh '
    })
});

app.use('/users', authMiddleware.requireAuth, userRouter);
app.use('/auth',authRouter);
app.use('/transfer', authMiddleware.requireAuth, transferRouter);




app.listen(port, function () {
    console.log('Server listening on port ' + port);
});