var express = require('express');
var router = express.Router();
var userController = require('../../api/controllers/user.controller.js');



router.get('/', userController.index);
router.post('/', userController.postCreate);
router.delete('/:id', userController.deleteItem);


module.exports = router;