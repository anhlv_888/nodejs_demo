var User = require('../../models/user.model.js');


module.exports.index = async function (req, res, next) {
    try {
        var users = await User.find();
        // users.foo();
        res.json(users);
    } catch (err) {
        next(err);
    }

};

module.exports.postCreate = async function (req, res) {
    var user = await User.create(req.body);
    res.json(user);
};

module.exports.deleteItem = async function (req, res) {
    var id = req.params.id;
    var user = await User.findOneAndRemove({
        _id : id
    });
    res.send('Delete success!');
};