module.exports.postCreate = function (req, res, next) {
    var errs = [];
    if (!req.body.name) {
        errs.push('Name is required !')
    }

    if (!req.body.phone) {
        errs.push('phone is required!')
    }

    if (!req.body.email) {
        errs.push('email is required!')
    }

    if (!req.body.password) {
        errs.push('password is required!')
    }
    if (errs.length) {
        res.render('users/create',{
            errs : errs,
            values : req.body
        });
        return;
    }
    next()
};