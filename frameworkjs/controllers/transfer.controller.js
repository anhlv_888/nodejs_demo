var db = require('../db.js');
var shortId = require('shortid');


module.exports.create = function (req, res, next) {
    res.render('transfer/create', {
        csrfToken: req.csrfToken()
    });

};

module.exports.postCreate = function (req, res, next) {
    var data = {
      id:   shortId.generate(),
        amount: parseInt(req.body.tien),
        accId: req.body.account,
        userId: req.signedCookies.userId
    };

    db.get('transfer').push(data).write();
    res.redirect('/transfer/create');

};