var db = require('../db.js');
var md5 = require('md5');


module.exports.login = function (req, res) {
    res.render('auth/login',{
        csrfToken: req.csrfToken()
    });
};

module.exports.postLogin = function (req, res) {
    var email  = req.body.email;
    var password = req.body.password;
    var user = db.get('users').find({ email : email}).value();
    if (!user) {
        res.render('auth/login',{
            errs : [
                'User does not exits.'
            ],
            values : res.body
        });
        return;
    }

    var hashedPassword = md5(password);

    if (user.password != hashedPassword ){
        res.render('auth/login',{
            errs: [
                'wrong password.'
            ],
            values : res.body
        });
        return;
    }
    res.cookie('userId',user.id,{
        signed : true
    });
    res.redirect('/users');

};