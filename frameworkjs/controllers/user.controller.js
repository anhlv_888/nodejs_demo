var db = require('../db.js');
var shortid = require('shortid');
var User = require('../models/user.model.js');


module.exports.index = function (req, res) {
    User.find().then(function (users) {
       res.render('users/index',{
           users: users
       })
    });

    // res.render('users/index', {
    //     users: db.get('users').value()
    // })
};

module.exports.search = async function (req, res) {
    var q = req.query.q;
    var users = await User.find();
    var matchUsers = users.filter(function (user) {
        return user.name.toLowerCase().indexOf(q.toLowerCase()) !== -1;
    });
    res.render('users/index', {
        users: matchUsers
    });

};


module.exports.getCreate =  function (req, res) {
    res.render('users/create',{
        // csrfToken: req.csrfToken()
    });
};

module.exports.postCreate = async function (req, res) {

    // req.body.id = shortid.generate();
    var avatar = req.file.path;
    console.log(avatar);

    avatar = avatar.split('/').slice(1).join('/');
    req.body.avatar = avatar;
    console.log(req.body);
    var user = await User.create(req.body);

    res.redirect('/users');

};

module.exports.view = async function (req, res) {
    var id = req.params.id;
     // User.findOne({ _id : id }).then(function(user){
     //     res.render('users/view',{
     //         user : user
     //     });
     // });

    var user = await User.findOne({_id : id});

    res.render('users/view',{
        user : user
    });


} ;